<?php

namespace App\Http\Controllers;

use App\Models\Hobby;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HobbyController extends Controller
{
    protected $user;


    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->guard()->user();

    }//end __construct()


    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hobbies = $this->user->hobbies()->get(['id', 'hobby', 'created_by']);
        return response()->json($hobbies->toArray());

    }//end index()


    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'hobby'     => 'required|string',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }

        $hobby            = new Hobby();
        $hobby->hobby     = $request->hobby;

        if ($this->user->hobbies()->save($hobby)) {
            return response()->json(
                [
                    'status' => true,
                    'hobby'   => $hobby,
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Oops, the hobby could not be saved.',
                ]
            );
        }

    }//end store()


    /*
     * Display the specified resource.
     *
     * @param  \App\Models\Hobby $hobby
     * @return \Illuminate\Http\Response
     */
    public function show(Hobby $hobby)
    {
        return $hobby;

    }//end show()


    /*
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Hobby         $hobby
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hobby $hobby)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'hobby'     => 'required|string',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }

        $hobby->hobby     = $request->hobby;

        if ($this->user->hobbies()->save($hobby)) {
            return response()->json(
                [
                    'status' => true,
                    'hobby'   => $hobby,
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Oops, the hobby could not be updated.',
                ]
            );
        }

    }//end update()


    /*
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hobby $hobby
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hobby $hobby)
    {
        if ($hobby->delete()) {
            return response()->json(
                [
                    'status' => true,
                    'hobby'   => $hobby,
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Oops, the hobby could not be deleted.',
                ]
            );
        }

    }//end destroy()


    protected function guard()
    {
        return Auth::guard();

    }//end guard()

}
