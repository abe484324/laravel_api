<?php

namespace App\Http\Controllers;

use App\Models\Gift;
use App\Models\Hobby;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class GiftController extends Controller
{
    protected $user;


    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->guard()->user();

    }//end __construct()

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gifts = $this->user->gifts()->get(['id', 'gift', 'created_by']);
        //Hobby::query()->join('user','user.id','hobby.created_by') 
        return response()->json($gifts->toArray());
    }


    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'gift'     => 'required|string',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }

        $gift            = new Gift();
        $gift->gift     = $request->gift;

        if ($this->user->gifts()->save($gift)) {
            return response()->json(
                [
                    'status' => true,
                    'gift'   => $gift,
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Oops, the gift could not be saved.',
                ]
            );
        }

    }//end store()

    /*
     * Display the specified resource.
     *
     * @param  \App\Models\Gift  $gift
     * @return \Illuminate\Http\Response
     */
    public function show(Gift $gift)
    {
        return $gift;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gift  $gift
     * @return \Illuminate\Http\Response
     */
    public function edit(Gift $gift)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gift  $gift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gift $gift)
    {
        $validator = Validator::make(
        $request->all(),
        [
            'gift'     => 'required|string',
        ]
    );

    if ($validator->fails()) {
        return response()->json(
            [
                'status' => false,
                'errors' => $validator->errors(),
            ],
            400
        );
    }

    $gift->gift     = $request->gift;

    if ($this->user->gifts()->save($gift)) {
        return response()->json(
            [
                'status' => true,
                'gift'   => $gift,
            ]
        );
    } else {
        return response()->json(
            [
                'status'  => false,
                'message' => 'Oops, the gift could not be updated.',
            ]
        );
    }

}//end update()

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gift  $gift
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gift $gift)
    {
        if ($gift->delete()) {
            return response()->json(
                [
                    'status' => true,
                    'gift'   => $gift,
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Oops, the gift could not be deleted.',
                ]
            );
        }

    }//end destroy()

    protected function guard()
    {
        return Auth::guard();

    }//end guard()
}
