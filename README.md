1. Install Laravel
2. Install JWT
3. Publish JWT Config
4. Generate JWT Secret
5. Configure Auth configuration
6. Implement JWTSubject interface in User model
7. Create API route
8. Create AuthController with login, register, refresh, logout and profile method
9. Create Todo Model with migration and resource TodoController
10. Migrate database
11. Modify UserFactory
12. Create and modify TodoFactory
13. Use of a Psy Shell Laravel Tinker
14. Create some dummy users and Todo data
15. Create some dummy users and Hobby data
16. Use a RESTful API client Postman
17. Test User login, register, refresh, logout and user profile
18. Test Todo view, create, modify and delete
19. Test Hobby view, create, modify and delete